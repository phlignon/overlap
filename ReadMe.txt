To compile the program, you need to install VS2015. The community (free) edition is fine: 
https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx

Then, all is required is to open the solution (sln file) that is at the root of the repository.

Compiling the solution will automatically compile and run the unit test project. If one of the unit tests fails, the compilation will automatically report a failure.

I have left a couple of text files that I have used to run the program to test the behaviour with different bad input, but the unit test also contains tests on bad inputs.