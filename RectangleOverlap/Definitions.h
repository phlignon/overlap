#pragma once

#include <vector>
#include <memory>

namespace overlap
{
	class Rectangle;
	typedef std::vector<std::shared_ptr<Rectangle>> Rectangles;
	typedef std::vector<unsigned int> Sources;
}