#pragma once

#include "Definitions.h"
namespace overlap
{
	class IRectangleReader
	{
	public:
		/*!
		@brief Reads/gets rectangle from a source and returns them.

		@return vector containing the rectangles read from the source

		@throw variable depending on the source

		*/
		virtual Rectangles ReadRectangles() = 0;
	};
}