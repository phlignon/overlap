#include "stdafx.h"
#include "OverlapSolver.h"
#include "Rectangle.h"
#include "IRectangleReader.h"

#include <memory>
#include <algorithm>
using namespace overlap;
using namespace std;

OverlapSolver::OverlapSolver()
{
}


OverlapSolver::~OverlapSolver()
{
}



/*!
@brief Call to get the list of starting rectangles from a source being passed.

@param[in] IRectangleReader source reader

@return void
*/
void OverlapSolver::ReadRectangles(IRectangleReader& reader)
{
	_sources.clear();
	Rectangles rectangles = reader.ReadRectangles();
	for (auto& rectangle : rectangles)
	{
		_sources.push_back(rectangle);
		rectangle->AddSource(_sources.size());
	}
}


/*!
@brief Finds the list of overlaps based on the rectangle that have been read.

Starts with finding overlaps between the source rectangles and themselves.
Then finds overlaps between the source rectangles and the result of the previous compute.
*/
void OverlapSolver::SolveOverlaps()
{
	_overlaps.clear();
	Rectangles lastRunResult = _sources;
	while (!lastRunResult.empty())
	{
		lastRunResult = FindOverlaps(lastRunResult);
		_overlaps.insert(end(_overlaps), begin(lastRunResult), end(lastRunResult));
	}
}

/*!
@brief Source rectangles

Returns the starting rectangles.

@return const reference vector containing the initial rectangles.
*/
const Rectangles& OverlapSolver::GetSources() const
{
	return _sources;
}


/*!
@brief Source rectangles

Returns the starting rectangles.

@return const reference vector containing the initial rectangles.
*/
const Rectangles& OverlapSolver::GetOverlaps() const
{
	return _overlaps;
}

/*!
@brief returns direct overlaps between a list of rectangles and the source rectangles

@param [in] rectangles with wich to find the overlaps

@return vector containing the overleap rectangles.
*/
Rectangles OverlapSolver::FindOverlaps(const Rectangles& lastRun) const
{
	Rectangles newRun;
	for (auto& overlap : lastRun)
	{
		Rectangles overlaps = FindOverlaps(*overlap);
		newRun.insert(end(newRun), begin(overlaps), end(overlaps));
	}
	return newRun;
}

/*!
@brief returns direct overlaps between a rectangle and the source rectangles

@param [in] rectangle with wich to find the overlaps

@return vector containing the overleap rectangles.
*/
Rectangles OverlapSolver::FindOverlaps(const Rectangle& existing) const
{
	Rectangles overlaps;
	for (auto& sourceRectangle: _sources)
	{
		if (sourceRectangle->FirstSource() < existing.FirstSource())
		{
			auto overlap = sourceRectangle->Overlap(existing);
			auto findRectangle = [&](shared_ptr<Rectangle> rect) {return (*overlap == *rect); };
				if ((overlap != nullptr) 
					&& (find_if(begin(GetOverlaps()), end(GetOverlaps()), findRectangle) == end(GetOverlaps())))
			{
				auto toAdd = shared_ptr<Rectangle>(move(overlap));
				overlaps.push_back(toAdd);
			}
		}
	}
	return overlaps;
}

/*!
@brief Calculate the overall surace covered by the rectangle. The function starts by adding the surface of all the rectangles.
Then we remove the surface of the overlaps that would have been counted twice. However, if there are common surfaces of the overlaps,
then we may have to do it again.
The principle is to remove from the result if there is an even number of sources for the overlap, and to add again if there is an odd
number of sources for the overlap.

@return Surface covered by the rectangles.
*/
int OverlapSolver::GetSurface() const
{
	int surface = 0;

	for (auto rect : _sources) surface += rect->Surface();

	for (auto rect : _overlaps)
	{ 
		int ratio = ((rect->GetSources().size() % 2) == 0) ? -1 : 1;
		surface += (rect->Surface() * ratio);
	}

	return surface;
}