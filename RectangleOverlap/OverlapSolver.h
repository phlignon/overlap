#pragma once

#include "Definitions.h"

namespace overlap
{
	class IRectangleReader;
	/*
	This class is the main class to find ovelaps. The typical use is:
	OverlapSolver solver;
	solver.ReadRectangler(---);		//reads the source rectangles from any source.
	solver.SolveOverlaps();			//stores the result in the _ovelaps member.
	solver.GetOverlaps();			//returns the vector of overlaps.
	*/
	class OverlapSolver
	{
	public:
		OverlapSolver();
		~OverlapSolver();

		void ReadRectangles(IRectangleReader&);
		void SolveOverlaps();
		const Rectangles& GetSources() const;
		const Rectangles& GetOverlaps() const;
		int GetSurface() const;

	private:
		Rectangles FindOverlaps(const Rectangles&) const;
		Rectangles FindOverlaps(const Rectangle&) const;

	private:
		Rectangles _sources;
		Rectangles _overlaps;
	};

}