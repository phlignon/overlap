#include "stdafx.h"
#include "Rectangle.h"

#include <algorithm>

using namespace overlap;
using namespace std;


Rectangle::Rectangle() : _topleft{ 0, 0 }, _size{ 0, 0 }
{
}

Rectangle::Rectangle(Point topleft, Point bottomright) : 
	_topleft{ min(topleft.x, bottomright.x), min(topleft.y, bottomright.y) },
	_size{ max(topleft.x, bottomright.x) - _topleft.x, max(topleft.y, bottomright.y) - _topleft.y }
{
}

Rectangle::Rectangle(Point topleft, Size size) :
	_topleft{ topleft.x, topleft.y }, _size{ size.width, size.height }
{
}



Rectangle::Rectangle(const Rectangle& rhs) :
	_topleft(rhs.Left(), rhs.Top()), _size(rhs.Width(), rhs.Height())
{
}



Rectangle::~Rectangle()
{
}

/*!
@brief return the overlap with another rectangle

returns the overlap between the current rectangle and the rectangle passed as parameter.
if there is no overlap, or if a rectangle is totally included in the other, we return 
nullptr. If there is an overlap, we merge the sources into the target rectangle.

@param [in] other rectangle

@return Unique pointer to overlap rectangle, or null if there is no overlap
*/
unique_ptr<Rectangle> Rectangle::Overlap(const Rectangle& other) const
{
	int top = max(Top(), other.Top());
	int bottom = min(Bottom(), other.Bottom());
	if (bottom < top) return nullptr;
		
	int left = max(Left(), other.Left());
	int right = min(Right(), other.Right());
	if (right < left) return nullptr;

	auto overlap = make_unique<Rectangle>(Point(left, top), Point(right, bottom));

	overlap->AddSources(GetSources());
	overlap->AddSources(other.GetSources());

	return overlap;
}

/*!
@brief Equatily operator

Returns true if the rectangles are the same (same coordinates and size).
The list of sources is ignored in the compare.

@param [in] rectangle with wich to check for equality.

@return bool: true if the rectangles are identical.
*/
bool Rectangle::operator==(const Rectangle& rhs) const
{
	return (Top() == rhs.Top())
		&& (Left() == rhs.Left())
		&& (Bottom() == rhs.Bottom())
		&& (Right() == rhs.Right())
		&& (_sources == rhs._sources)
		;
}

/*!
@brief returns the first of the sources, if any

@return index (1 based) of the first source, or 0 if none.
*/
int Rectangle::FirstSource() const
{
	if (GetSources().empty())
	{
		return 0;
	}
	else
	{
		return GetSources()[0];
	}
}

/*!
@brief Adds a source

The source is added only if it does not already exists. Don't create duplicates.

@param [in] unsigned int: index of the source to add
*/
void Rectangle::AddSource(unsigned int index)
{
	if (!ContainsSource(index))
	{
		_sources.push_back(index);
	}
}

/*!
@brief Verify if the rectangles contains a source.

@param [in] unsigned int: index of the source to look for.

@return bool: true if it contains the source, false othewise.
*/
bool Rectangle::ContainsSource(unsigned int index) const
{
	return (find(begin(_sources), end(_sources), index) != end(_sources));
}

/*!
@brief Adds a list of sources to the current rectangle.

No duplicate will be added.

@param [in] vector of sources
*/
void Rectangle::AddSources(const Sources& sources)
{
	for (auto& index : sources)
	{
		AddSource(index);
	}
}

/*!
@brief Returns the surface of the Rectangle.

No duplicate will be added.

@param [in] vector of sources
*/unsigned int Rectangle::Surface() const
{
	return Height() * Width();
}