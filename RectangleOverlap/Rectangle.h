#pragma once

#include <memory>
#include "Definitions.h"

namespace overlap
{
	struct Point
	{
		Point(unsigned int xinput, unsigned int yinput) : x(xinput), y(yinput) {}
		unsigned int x;
		unsigned int y;
	};

	struct Size
	{
		Size(unsigned int w, unsigned int h) : width(w), height(h) {}
		unsigned int width;
		unsigned int height;
	};

	/*
	This is a simple rectangle class with the addition of a source id information.
	An overlap rectangle will store the list or sources for creating the overlap.
	*/
	class Rectangle
	{
	public:
		Rectangle();
		Rectangle(Point topleft, Point bottomright);
		Rectangle(Point topleft, Size size);
		Rectangle(const Rectangle& rhs);
		virtual ~Rectangle();

		bool operator==(const Rectangle& rhs) const;

		unsigned int Top() const { return _topleft.y; }
		unsigned int Left() const { return _topleft.x; }
		unsigned int Bottom() const { return _topleft.y + _size.height; }
		unsigned int Right() const { return _topleft.x + _size.width; }
		unsigned int Width() const { return _size.width; }
		unsigned int Height() const { return _size.height; }
		unsigned int Surface() const;

		std::unique_ptr<Rectangle> Overlap(const Rectangle& other) const;

		void AddSource(unsigned int index);
		void AddSources(const Sources& sources);

		int FirstSource() const;
		const Sources& GetSources() const { return _sources; }
		bool ContainsSource(unsigned int index) const;

	private:
		Point _topleft;
		Size _size;

		Sources _sources;
	};

}