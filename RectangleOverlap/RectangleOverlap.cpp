// RectangleOverlap.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

#include "Definitions.h"
#include "RectangleReaderJSon.h"
#include "OverlapSolver.h"
#include "RectanglePrinter.h"

using namespace std;
using namespace overlap;

int main(int argc, char* argv[])
{ 
	if (argc < 2)
	{
		cout << "Missing the file name!\n";

		return -1;
	}


	try
	{
		RectangleReaderJSon reader{ argv[1] };
		OverlapSolver solver;
		solver.ReadRectangles(reader);
		RectanglePrinter::PrintSources(solver.GetSources());
		solver.SolveOverlaps();
		RectanglePrinter::PrintOverlaps(solver.GetOverlaps());
		cout << "The overall surface covered by the rectangles is: " << solver.GetSurface() << endl;
	}
	catch(domain_error& error) 
	{
		cout << "A domain error was encountered: " << error.what() << endl;
	}
	catch(invalid_argument& error)
	{
		cout << "An invalid argument error was encountered: " << error.what() << endl;
	}
	catch (out_of_range& error) 
	{
		cout << "An out of range error was encountered: " << error.what() << endl;
	}

    return 0;
}
