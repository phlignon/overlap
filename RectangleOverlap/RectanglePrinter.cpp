#include "stdafx.h"
#include "RectanglePrinter.h"
#include "Rectangle.h"

#include <sstream>
#include <iostream>


using namespace overlap;
using namespace std;


/*!
@brief Print the input rectangles for the project

@param [in] const reference vector of source rectangles
*/
void RectanglePrinter::PrintSources(const Rectangles& rectangles)
{
	cout << "Input:" << endl;
	for (auto& rectangle: rectangles)
	{
		cout << "\t" << rectangle->GetSources()[0] << ": Rectangle at " << RectangleToString(*rectangle) << "." << endl;
	}
}


/*!
@brief Print the overlap rectangles for the project

@param [in] const reference vector of overlap rectangles
*/
void RectanglePrinter::PrintOverlaps(const Rectangles& overlaps)
{
	cout << "Overlaps" << endl;
	if (overlaps.empty())
	{
		cout << "None" << endl;
	}
	else
	{
		for (auto& overlap : overlaps)
		{
			cout << "Between rectangle " << SourcesToString(overlap->GetSources())
				<< " at " << RectangleToString(*overlap) << "." << endl;
		}
	}
}

/*!
@brief Formats the sources into a string.

@param [in] const reference vector of source indexes.

@return string: list of sources.
*/
string RectanglePrinter::SourcesToString(const Sources& indexes)
{
	ostringstream oss;
	size_t count = indexes.size();
	size_t oneRemainingIndex = count - 2;
	for (size_t index = 0; index < count; ++index)
	{
		oss << indexes[index];
		if (index == oneRemainingIndex) oss << " and ";
		else if (index < oneRemainingIndex) oss << ", ";
	}
	return oss.str();
}

/*!
@brief Format a rectangle into a string

@param [in] Const reference of a rectangle

@return string: rectangle coordinates as a string.
*/
string RectanglePrinter::RectangleToString(const Rectangle& rectangle)
{
	ostringstream oss;
	oss << "(" << rectangle.Left() << ", " << rectangle.Top();
	oss << ") , w=" << rectangle.Width() << ", h= " << rectangle.Height();
	return oss.str();
}
