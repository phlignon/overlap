#pragma once


#include "Definitions.h"
namespace overlap
{
	class RectanglePrinter
	{
	public:
		static void PrintSources(const Rectangles&);
		static void PrintOverlaps(const Rectangles&);

	private:
		static std::string SourcesToString(const Sources&);
		static std::string RectangleToString(const Rectangle&);
	};

}