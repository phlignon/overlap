#include "stdafx.h"
#include "RectangleReaderJSon.h"

#include "Rectangle.h"
#include <fstream>
#include "json.hpp"
using json = nlohmann::json;


using namespace overlap;
using namespace std;

RectangleReaderJSon::RectangleReaderJSon(string filename) : _filename(filename)
{
}


/*!
@brief reads rectangles from a json file

@throw std::invalid argument if the file cannot be read
std::invalid_argument, std::domain_error, std::out_of_range may be thrown by the json parser

@return vector of rectangles read from the json file
*/
Rectangles RectangleReaderJSon::ReadRectangles()
{
	json source;
	ifstream jsonfile{ _filename, std::ifstream::in };
	if (!jsonfile.is_open() || !jsonfile.good() || jsonfile.fail())
	{
		throw std::invalid_argument("Failed to open the file!");
	}

	Rectangles result = ReadRectangles(jsonfile);
	jsonfile.close();

	return result;
}


/*!
@brief reads the rectangles from a JSon formatted input source

@param [in] istream reference to JSon data

@throw std::invalid_argument, std::domain_error, std::out_of_range may be thrown by the json parser

@return vector of rectangles read from the JSon source.
*/
Rectangles RectangleReaderJSon::ReadRectangles(std::istream& input) const
{
	json source;
	input >> source;
	if (source.find("rects") != source.end())
	{
		return ExtractRectangles(source["rects"]);
	}
	else
	{
		return Rectangles{};
	}
}

/*!
@brief Extract the rectangle information from the JSon array.

@param [in] JSon object that should be an array of rectangle data.

@throw std::invalid_argument, std::domain_error, std::out_of_range may be thrown by the json parser

@return vector of rectangles read from the JSon array.
*/
Rectangles RectangleReaderJSon::ExtractRectangles(const json& sourceArray) const
{
	Rectangles rectangles;
	rectangles.reserve(sourceArray.size());
	for (auto& element : sourceArray)
	{
		if (   element.find("x") != element.end()
			&& element.find("y") != element.end()
			&& element.find("h") != element.end()
			&& element.find("w") != element.end()
			)
		{
			unsigned int x = element["x"];
			unsigned int y = element["y"];
			unsigned int h = element["h"];
			unsigned int w = element["w"];
			rectangles.push_back(make_shared<Rectangle>(Point{ x, y }, Size{ w, h }));
		}
	}
	return rectangles;
}

