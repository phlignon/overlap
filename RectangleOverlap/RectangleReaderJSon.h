#pragma once
#include "IRectangleReader.h"
#include "json.hpp"

namespace overlap
{
	class RectangleReaderJSon : public IRectangleReader
	{
	public:
		RectangleReaderJSon(std::string filename);
		Rectangles ReadRectangles() override;
		Rectangles ReadRectangles(std::istream& input) const;

	private:
		Rectangles ExtractRectangles(const nlohmann::json& sourceArray) const;

	private:
		std::string _filename;
	};
}