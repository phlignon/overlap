#include "stdafx.h"
#include "Base_Test.h"

#include <iostream>
using namespace std;

Base_Test::Base_Test() : _failureCount(0), _result(result::success)
{
}


void Base_Test::Failed(string errorMessage)
{
	++_failureCount;
	_result = result::failure;
	cout << _failureCount << ": " << errorMessage << endl;
}

Base_Test::result Base_Test::GetResult() const
{
	return _result;
}