#pragma once
#include <string>

class Base_Test
{
public:
	enum class result { success, failure };

	Base_Test();
	virtual result Test() = 0;

	int Failures() const { return _failureCount; }

protected:
	void Failed(std::string errorMessage);
	result GetResult() const;

private:
	result _result;
	int _failureCount;
};

