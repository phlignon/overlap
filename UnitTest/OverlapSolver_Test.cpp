#include "stdafx.h"
#include "OverlapSolver_Test.h"

#include "IRectangleReader.h"
#include "Rectangle.h"
#include "OverlapSolver.h"

#include <sstream>

using namespace std;
using namespace overlap;

class ReaderStub : public IRectangleReader
{
public:
	Rectangles ReadRectangles() override { return _rectangles; }
	Rectangles _rectangles;
};

OverlapSolver_Test::OverlapSolver_Test()
{
}



Base_Test::result OverlapSolver_Test::Test()
{
	TestReadingEmptyList();
	TestReading();
	TestSolve0();
	TestSolve1();
	TestSolve2();
	TestSolve3();
	TestSolve4();

	TestSurface1();
	TestSurface2NoOverlap();
	TestSurface2();
	TestSurface3();
	TestSurface4();

	return GetResult();
}

void OverlapSolver_Test::TestReadingEmptyList()
{
	ReaderStub reader;
	OverlapSolver solver;
	solver.ReadRectangles(reader);

	if (solver.GetSources().size() != 0)
	{
		Failed("The list of rectangles should be empty.");
	}
}

void OverlapSolver_Test::TestReading()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>());
	reader._rectangles.push_back(make_shared<Rectangle>());

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	if (solver.GetSources().size() != 2)
	{
		Failed("Did not read the correct amount of rectangles.");
	}
}

void OverlapSolver_Test::TestSolve0()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 5, 5 }, Point{ 15, 15 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 30, 30 }, Point{ 20, 20 }));

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetOverlaps().size() != 0)
	{
		Failed("No overlap should have been found.");
	}
}

void OverlapSolver_Test::TestSolve1()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 5, 5 }, Point{ 15, 15 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 10 }, Point{ 20, 20 }));

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetOverlaps().size() != 1)
	{
		Failed("Failed to find 1 overlaps.");
	}
}

void OverlapSolver_Test::TestSolve2()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 5, 5 }, Point{ 15, 15 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 10 }, Point{ 20, 20 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 16, 16 }, Point{ 23, 23 }));

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetOverlaps().size() != 2)
	{
		Failed("Failed to find 2 overlaps.");
	}
}

void OverlapSolver_Test::TestSolve3()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 5, 5 }, Point{ 15, 15 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 10 }, Point{ 20, 20 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 7, 12 }, Point{ 17, 30 }));

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetOverlaps().size() != 4)
	{
		Failed("Failed to find 4 overlaps.");
	}
}

void OverlapSolver_Test::TestSolve4()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 0, 0 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 0 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 0, 10 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 10 }, Size{ 30, 30 }));

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetOverlaps().size() != 11)
	{
		ostringstream oss;
		oss << "Failed to find 11 overlaps. Instead found: " << solver.GetOverlaps().size();
		Failed(oss.str());
		//Failed("Failed to find 11 overlaps.");
	}
}

void OverlapSolver_Test::TestSurface1()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 0, 0 }, Size{ 30, 30 }));
	int expectedSurface = 900;

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetSurface() != expectedSurface)
	{
		ostringstream oss;
		oss << "Failed to find a surface of " << expectedSurface << ". Instead found: " << solver.GetSurface();
		Failed(oss.str());
	}
}

void OverlapSolver_Test::TestSurface2NoOverlap()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 0, 0 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 40, 0 }, Size{ 30, 30 }));
	int expectedSurface = 1800;

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetSurface() != expectedSurface)
	{
		ostringstream oss;
		oss << "Failed to find a surface of " << expectedSurface << ". Instead found: " << solver.GetSurface();
		Failed(oss.str());
	}
}

void OverlapSolver_Test::TestSurface2()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 0, 0 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 10 }, Size{ 30, 30 }));
	int expectedSurface = 1400;

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetSurface() != expectedSurface)
	{
		ostringstream oss;
		oss << "Failed to find a surface of " << expectedSurface << ". Instead found: " << solver.GetSurface();
		Failed(oss.str());
	}
}

void OverlapSolver_Test::TestSurface3()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 0, 0 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 10 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 30, 30 }, Size{ 30, 30 }));
	int expectedSurface = 2200;

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetSurface() != expectedSurface)
	{
		ostringstream oss;
		oss << "Failed to find a surface of " << expectedSurface << ". Instead found: " << solver.GetSurface();
		Failed(oss.str());
	}
}

void OverlapSolver_Test::TestSurface4()
{
	ReaderStub reader;
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 0, 0 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 0 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 0, 10 }, Size{ 30, 30 }));
	reader._rectangles.push_back(make_shared<Rectangle>(Point{ 10, 10 }, Size{ 30, 30 }));
	int expectedSurface = 1600;

	OverlapSolver solver;
	solver.ReadRectangles(reader);
	solver.SolveOverlaps();
	if (solver.GetSurface() != expectedSurface)
	{
		ostringstream oss;
		oss << "Failed to find a surface of " << expectedSurface << ". Instead found: " << solver.GetSurface();
		Failed(oss.str());
	}
}

