#pragma once
#include "Base_Test.h"

class OverlapSolver_Test : public Base_Test
{
public:
	OverlapSolver_Test();
	result Test() override;

private:
	void TestReadingEmptyList();
	void TestReading();
	void TestSolve0();
	void TestSolve1();
	void TestSolve2();
	void TestSolve3();
	void TestSolve4();

	void TestSurface1();
	void TestSurface2NoOverlap();
	void TestSurface2();
	void TestSurface3();
	void TestSurface4();
};

