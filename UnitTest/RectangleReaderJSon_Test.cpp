#include "stdafx.h"
#include "RectangleReaderJSon_Test.h"

#include "RectangleReaderJSon.h"

#include <sstream>

using namespace std;
RectangleReaderJSon_Test::RectangleReaderJSon_Test()
{
}


Base_Test::result RectangleReaderJSon_Test::Test()
{
	TestGoodJSon();
	TestEmptyString();
	TestEmptyJSon();
	TestBadFormat();
	TestEmptyList();
	TestMissingParameter();
	TestMissingRects();
	return GetResult();
}


void RectangleReaderJSon_Test::TestGoodJSon()
{ 
	string str = R"JSon({
						"rects": [
						{"x": 100, "y": 100, "w": 250, "h": 80 }, 
						{"x": 120, "y": 200, "w": 250, "h": 150 }, 
						{"x": 160, "y": 140, "w": 350, "h": 190 }
						]
						})JSon";
	istringstream iss{ str };
	overlap::RectangleReaderJSon reader{ "" };
	overlap::Rectangles rects = reader.ReadRectangles(iss);

	if (rects.size() != 3) Failed("Failed to read rectangles");
}

void RectangleReaderJSon_Test::TestEmptyString()
{
	string str = "";
	istringstream iss{ str };
	overlap::RectangleReaderJSon reader{ "" };
	try
	{
		overlap::Rectangles rects = reader.ReadRectangles(iss);
		Failed("It should have thrown an exception");
	}
	catch (invalid_argument&)
	{
		//expected exeption
	}
	catch (...)
	{
		Failed("Unexpected exception");
	}
}

void RectangleReaderJSon_Test::TestEmptyJSon()
{
	string str = R"JSon({
						})JSon";
	istringstream iss{ str };
	overlap::RectangleReaderJSon reader{ "" };
	overlap::Rectangles rects = reader.ReadRectangles(iss);
	if (!rects.empty()) Failed("Did not expect any rectangle");
}

void RectangleReaderJSon_Test::TestBadFormat() 
{
	string str = R"JSon({
						"rects": [
						{"x": 100, "y": 100, "w": 250, "h": 80 }, 
						{"x": 120, "y": 200, "w": 250, "h": 150 }, 
						{"x": 160, "y": 140, 
			"w": 350, "h": 190 }
						]
						})JSon";
	istringstream iss{ str };
	overlap::RectangleReaderJSon reader{ "" };
	overlap::Rectangles rects = reader.ReadRectangles(iss);

	try
	{
		overlap::Rectangles rects = reader.ReadRectangles(iss);
		Failed("It should have thrown an exception");
	}
	catch (invalid_argument&)
	{
		//expected exeption
	}
	catch (...)
	{
		Failed("Unexpected exception");
	}
}

void RectangleReaderJSon_Test::TestEmptyList()
{
	string str = R"JSon({
						"rects": [
						]
						})JSon";
	istringstream iss{ str };
	overlap::RectangleReaderJSon reader{ "" };
	overlap::Rectangles rects = reader.ReadRectangles(iss);

	if (!rects.empty()) Failed("The list should be empty");
}

void RectangleReaderJSon_Test::TestMissingParameter()
{
	string str = R"JSon({
						"rects": [
						{"x": 100, "y": 100, "w": 250, "h": 80 }, 
						{"x": 120, "y": 200 }, 
						{"x": 160, "y": 140, "w": 350, "h": 190 }
						]
						})JSon";
	istringstream iss{ str };
	overlap::RectangleReaderJSon reader{ "" };
	overlap::Rectangles rects = reader.ReadRectangles(iss);

	if (rects.size() < 2) Failed("Failed to read the rectangles while testing missing parameter.");
	else if (rects.size() > 2) Failed("One rectangle is invalid and should not have been added.");
}

void RectangleReaderJSon_Test::TestMissingRects()
{
	string str = R"JSon({
						"rect": {"x": 100, "y": 100, "w": 250, "h": 80 }, 
						"rect": {"x": 160, "y": 140, "w": 350, "h": 190 }
						})JSon";
	istringstream iss{ str };
	overlap::RectangleReaderJSon reader{ "" };
	overlap::Rectangles rects = reader.ReadRectangles(iss);

	if (!rects.empty()) Failed("not the expected format, the list should be empty");
}
