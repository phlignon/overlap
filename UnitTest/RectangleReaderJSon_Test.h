#pragma once
#include "Base_Test.h"

class RectangleReaderJSon_Test : public Base_Test
{
public:
	RectangleReaderJSon_Test();
	Base_Test::result Test() override;

private:
	void TestGoodJSon();
	void TestEmptyString();
	void TestEmptyJSon();
	void TestBadFormat();
	void TestEmptyList();
	void TestMissingParameter();
	void TestMissingRects();
};

