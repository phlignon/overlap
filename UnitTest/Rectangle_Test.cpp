#include "stdafx.h"
#include "Rectangle_Test.h"

#include "Rectangle.h"

using namespace overlap;

Rectangle_Test::Rectangle_Test()
{
}

Base_Test::result Rectangle_Test::Test()
{
	TestDefaultConstructor();
	TestConstructorOrderedParameters();
	TestconstructorInvertedParameters();
	TestCopyConstructor();
	TestWidth();
	TestHeight();
	TestEqualityOperator_Same();
	TestEqualityOperator_Different();
	TestTopLeftOverlap();
	TestTopRightOverlap();
	TestBottomLeftOverlap();
	TestBottomRightOverlap();
	TestLeftOverlap();
	TestTopOverlap();
	TestFullInclusion();
	TestNoOverlap();

	TestAddSource();
	TestAddDuplicateSource();
	TestAddSources();
	TestAddSourcesWithDuplicates();
	TestContainSource_NotInList();
	TestCOntainSource_InList();

	TestSurface();
	TestSurfaceEmpty();

	return GetResult();
}

void Rectangle_Test::TestDefaultConstructor()
{
	Rectangle rect;
	if (rect.Top() != 0
		|| rect.Left() != 0
		|| rect.Bottom() != 0
		|| rect.Right() != 0
		)
	{
		Failed("Default constructor failed");
	}
}

void Rectangle_Test::TestConstructorOrderedParameters()
{
	overlap::Point topleft{ 10, 10 }, bottomright{ 20, 20 };
	Rectangle rect{ topleft, bottomright };

	if (rect.Top() != topleft.y
		|| rect.Left() != topleft.x
		|| rect.Bottom() != bottomright.y
		|| rect.Right() != bottomright.x
		)
	{
		Failed("Failed with ordered parameters");
	}
}

void Rectangle_Test::TestconstructorInvertedParameters()
{
	overlap::Point topleft{ 10, 10 }, bottomright{ 20, 20 };
	Rectangle rect{ bottomright, topleft };

	if (rect.Top() != topleft.y
		|| rect.Left() != topleft.x
		|| rect.Bottom() != bottomright.y
		|| rect.Right() != bottomright.x
		)
	{
		Failed("Failed with inverted parameters");
	}
}


void Rectangle_Test::TestCopyConstructor()
{
	overlap::Point topleft{ 10, 10 }, bottomright{ 20, 20 };
	Rectangle source{ bottomright, topleft };
	Rectangle destination(source);

	if (source.Top() != destination.Top()
		|| source.Left() != destination.Left()
		|| source.Bottom() != destination.Bottom()
		|| source.Right() != destination.Right()
		)
	{
		Failed("Failed copy constructor");
	}
}

void Rectangle_Test::TestWidth()
{
	Rectangle r1{ overlap::Point{ 10, 10 }, overlap::Point{ 30, 20 } };
	Rectangle r2{ overlap::Point{ 100, 0 }, overlap::Point{ 220, 1000 } };
	Rectangle r3{ overlap::Point{ 100, 500 }, overlap::Point{ 20, 20 } };
	Rectangle r4{ overlap::Point{ 10, 20 }, overlap::Point{ 10, 20 } };

	if (r1.Width() != 20) Failed("first width is incorrect");
	if (r2.Width() != 120) Failed("second width is incorrect");
	if (r3.Width() != 80) Failed("third width is incorrect");
	if (r4.Width() != 0) Failed("fourth width is incorrect");
}

void Rectangle_Test::TestHeight()
{
	Rectangle r1{ overlap::Point{ 10, 10 }, overlap::Point{ 20, 20 } };
	Rectangle r2{ overlap::Point{ 10, 0 }, overlap::Point{ 20, 1000 } };
	Rectangle r3{ overlap::Point{ 10, 500 }, overlap::Point{ 20, 20 } };
	Rectangle r4{ overlap::Point{ 10, 20 }, overlap::Point{ 20, 20 } };
	Rectangle r5{ Point{10, 10}, Size(50, 123) };

	if (r1.Height() != 10) Failed("first height is incorrect");
	if (r2.Height() != 1000) Failed("second height is incorrect");
	if (r3.Height() != 480) Failed("third height is incorrect");
	if (r4.Height() != 0) Failed("fourth height is incorrect");
	if (r5.Height() != 123) Failed("Fifth height is incorrect");
}

void Rectangle_Test::TestEqualityOperator_Same()
{
	Rectangle first { overlap::Point{ 10, 10 }, overlap::Point{ 20, 20} };
	Rectangle second{ overlap::Point{ 10, 10 }, overlap::Point{ 20, 20} };

	if (!(first == second))
	{
		Failed("Operator == failed when rectangles are the same");
	}
}

void Rectangle_Test::TestEqualityOperator_Different()
{
	Rectangle first { overlap::Point{ 10, 10 }, overlap::Point{ 20, 20} };
	Rectangle second{ overlap::Point{ 11, 10 }, overlap::Point{ 20, 20} };

	if (first == second)
	{
		Failed("Operator == failed when rectangles are different");
	}
}


void Rectangle_Test::TestTopLeftOverlap()
{
	Rectangle first   { overlap::Point{ 10, 10 }, overlap::Point{ 20, 20} };
	Rectangle second  { overlap::Point{ 5, 5 }, overlap::Point{ 15, 15 }};
	Rectangle expected{ overlap::Point{ 10, 10}, overlap::Point{ 15, 15 } };

	auto result = first.Overlap(second);
	auto result2 = second.Overlap(first);

	if (result == nullptr) Failed("TestTopleftOverlap: Failed to find first overlap");
	else if (result2 == nullptr) Failed("TestTopleftOverlap: Failed to find second overlap");
	else if (!(*result == expected)) Failed("TestTopLeftOverlap: First result not as expepcted");
	else if (!(*result2 == expected)) Failed("TestTopLeftOverlap: second result not as expepcted");
	else if (!(*result == *result2)) Failed("TestTopLeftOverlap: First result should equal second result");
}


void Rectangle_Test::TestTopRightOverlap()
{
	Rectangle first   { overlap::Point{ 10, 10}, overlap::Point{ 20, 20 }};
	Rectangle second  { overlap::Point{ 5, 15 }, overlap::Point{ 15, 25 } };
	Rectangle expected{ overlap::Point{ 10, 15 }, overlap::Point{ 15, 20 } };

	auto result = first.Overlap(second);
	auto result2 = second.Overlap(first);

	if (result == nullptr) Failed("TestTopRightOverlap: Failed to find first overlap");
	else if (result2 == nullptr) Failed("TestTopRightOverlap: Failed to find second overlap");
	else if (!(*result == expected)) Failed("TestTopRightOverlap: First result not as expepcted");
	else if (!(*result2 == expected)) Failed("TestTopRightOverlap: second result not as expepcted");
	else if (!(*result == *result2)) Failed("TestTopRightOverlap: First result should equal second result");
}

void Rectangle_Test::TestBottomLeftOverlap()
{
	Rectangle first{ overlap::Point{ 10, 10 }, overlap::Point{ 20, 20 } };
	Rectangle second{ overlap::Point{ 15, 5 }, overlap::Point{ 25, 15 } };
	Rectangle expected{ overlap::Point{ 15, 10 }, overlap::Point{ 20, 15 } };

	auto result = first.Overlap(second);
	auto result2 = second.Overlap(first);

	if (result == nullptr) Failed("TestBottomLeftOverlap: Failed to find first overlap");
	else if (result2 == nullptr) Failed("TestBottomLeftOverlap: Failed to find second overlap");
	else if (!(*result == expected)) Failed("TestBottomLeftOverlap: First result not as expepcted");
	else if (!(*result2 == expected)) Failed("TestBottomLeftOverlap: second result not as expepcted");
	else if (!(*result == *result2)) Failed("TestBottomLeftOverlap: First result should equal second result");
}

void Rectangle_Test::TestBottomRightOverlap()
{
	Rectangle first{ overlap::Point{ 10, 10 }, overlap::Point{ 20, 20 } };
	Rectangle second{ overlap::Point{ 15, 15 }, overlap::Point{ 25, 25 } };
	Rectangle expected{ overlap::Point{ 15, 15 }, overlap::Point{ 20, 20 } };

	auto result = first.Overlap(second);
	auto result2 = second.Overlap(first);

	if (result == nullptr) Failed("TestBottomRightOverlap: Failed to find first overlap");
	else if (result2 == nullptr) Failed("TestBottomRightOverlap: Failed to find second overlap");
	else if (!(*result == expected)) Failed("TestBottomRightOverlap: First result not as expepcted");
	else if (!(*result2 == expected)) Failed("TestBottomRightOverlap: second result not as expepcted");
	else if (!(*result == *result2)) Failed("TestBottomRightOverlap: First result should equal second result");
}

void Rectangle_Test::TestLeftOverlap()
{
	Rectangle first{ overlap::Point{ 10, 10 }, overlap::Point{ 20, 20 } };
	Rectangle second{ overlap::Point{ 12, 5 }, overlap::Point{ 18, 15 } };
	Rectangle expected{ overlap::Point{ 12, 10 }, overlap::Point{ 18, 15 } };

	auto result = first.Overlap(second);
	auto result2 = second.Overlap(first);

	if (result == nullptr) Failed("TestLeftOverlap: Failed to find first overlap");
	else if (result2 == nullptr) Failed("TestLeftOverlap: Failed to find second overlap");
	else if (!(*result == expected)) Failed("TestLeftOverlap: First result not as expepcted");
	else if (!(*result2 == expected)) Failed("TestLeftOverlap: second result not as expepcted");
	else if (!(*result == *result2)) Failed("TestLeftOverlap: First result should equal second result");
}

void Rectangle_Test::TestTopOverlap()
{
	Rectangle first{ overlap::Point{ 10, 10 }, overlap::Point{ 20, 20 } };
	Rectangle second{ overlap::Point{ 5, 12 }, overlap::Point{ 15, 18 } };
	Rectangle expected{ overlap::Point{ 10, 12 }, overlap::Point{ 15, 18 } };

	auto result = first.Overlap(second);
	auto result2 = second.Overlap(first);

	if (result == nullptr) Failed("TestTopOverlap: Failed to find first overlap");
	else if (result2 == nullptr) Failed("TestTopOverlap: Failed to find second overlap");
	else if (!(*result == expected)) Failed("TestTopOverlap: First result not as expepcted");
	else if (!(*result2 == expected)) Failed("TestTopOverlap: second result not as expepcted");
	else if (!(*result == *result2)) Failed("TestTopOverlap: First result should equal second result");
}

void Rectangle_Test::TestFullInclusion()
{
	Rectangle first{ overlap::Point{ 10, 10 }, overlap::Point{ 20, 20 } };
	Rectangle second{ overlap::Point{ 12, 12 }, overlap::Point{ 18, 18 } };
	Rectangle expected{second};

	auto result = first.Overlap(second);
	auto result2 = second.Overlap(first);

	if (result == nullptr) Failed("TestFullInclusion: One overlap should have been found as one is included in the other.");
	else if (result2 == nullptr) Failed("TestFullInclusion: same overlap should have been found due to inclusion.");
}

void Rectangle_Test::TestNoOverlap()
{
	Rectangle rect{ overlap::Point{ 10, 10}, overlap::Point{ 20, 20 } };
	Rectangle first{ overlap::Point{ 5, 5 }, overlap::Point{ 15, 9 } };
	Rectangle second{ overlap::Point{ 5, 5 }, overlap::Point{ 9, 15 } };
	Rectangle third{ overlap::Point{ 12, 5 }, overlap::Point{ 15, 9 } };
	Rectangle fourth{ overlap::Point{ 25, 15 }, overlap::Point{ 35, 19} };

	if (rect.Overlap(first) != nullptr) Failed("TestNoOverlap: first failed");
	if (rect.Overlap(second) != nullptr) Failed("TestNoOverlap: first failed");
	if (rect.Overlap(third) != nullptr) Failed("TestNoOverlap: first failed");
	if (rect.Overlap(fourth) != nullptr) Failed("TestNoOverlap: first failed");
}

void Rectangle_Test::TestAddSource()
{
	size_t count = 5;
	Rectangle o;
	for (size_t i = 0; i < count; ++i) o.AddSource(i);
	if (o.GetSources().size() != count)
	{
		Failed("Failed to add sources.");
	}
}


void Rectangle_Test::TestAddDuplicateSource()
{
	size_t count = 5;
	Rectangle o;
	for (size_t i = 0; i < count; ++i) o.AddSource(i);
	for (size_t i = 0; i < count; ++i) o.AddSource(i);

	if (o.GetSources().size() < count)
	{
		Failed("Failed to add sources.");
	}
	else if (o.GetSources().size() > count)
	{
		Failed("Failed to identify duplicate sources.");
	}
}

void Rectangle_Test::TestAddSources()
{
	size_t count = 5;
	Rectangle o;
	for (size_t i = 0; i < count; ++i) o.AddSource(i);

	Sources sources{ 8, 9, 10 };
	o.AddSources(sources);
	if (o.GetSources().size() != (count + sources.size()))
	{
		Failed("Failed to add sources.");
	}
}

void Rectangle_Test::TestAddSourcesWithDuplicates()
{
	size_t count = 5;
	Rectangle o;
	for (size_t i = 0; i < count; ++i) o.AddSource(i);

	Sources sources{ 2, 3, 8, 9, 10 };
	o.AddSources(sources);
	if (o.GetSources().size() < (count + 3))
	{
		Failed("Failed to add sources.");
	}
	else if (o.GetSources().size() > (count + 3))
	{
		Failed("Failed to ignore duplicate while adding sources.");
	}
}

void Rectangle_Test::TestContainSource_NotInList()
{
	size_t count = 5;
	Rectangle o;
	for (size_t i = 2; i < count; ++i) o.AddSource(i);
	if (o.ContainsSource(1) ||
		o.ContainsSource(10))
	{
		Failed("Unexpected source found.");
	}
}

void Rectangle_Test::TestCOntainSource_InList()
{
	size_t count = 5;
	Rectangle o;
	for (size_t i = 0; i < count; ++i) o.AddSource(i);
	if (!o.ContainsSource(1) ||
		!o.ContainsSource(4))
	{
		Failed("Expected source not found.");
	}
}

void Rectangle_Test::TestSurface()
{
	Rectangle rect{ overlap::Point{10, 10}, overlap::Size{20, 20} };
	if (rect.Surface() != 400) Failed("Incorrect surface");
}

void Rectangle_Test::TestSurfaceEmpty()
{
	Rectangle rect{ overlap::Point{10, 10}, overlap::Size{0, 10} };
	if (rect.Surface() != 0) Failed("Surface should be zero");
}