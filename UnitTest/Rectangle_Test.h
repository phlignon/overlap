#pragma once
#include "Base_Test.h"

class Rectangle_Test : public Base_Test
{
public:
	Rectangle_Test();
	Base_Test::result Test() override;

private:
	void TestDefaultConstructor();
	void TestConstructorOrderedParameters();
	void TestconstructorInvertedParameters();
	void TestCopyConstructor();

	void TestEqualityOperator_Same();
	void TestEqualityOperator_Different();

	void TestWidth();
	void TestHeight();

	void TestTopLeftOverlap();
	void TestTopRightOverlap();
	void TestBottomLeftOverlap();
	void TestBottomRightOverlap();
	void TestLeftOverlap();
	void TestTopOverlap();
	void TestFullInclusion();
	void TestNoOverlap();

	void TestAddSource();
	void TestAddDuplicateSource();
	void TestAddSources();
	void TestAddSourcesWithDuplicates();
	void TestContainSource_NotInList();
	void TestCOntainSource_InList();

	void TestSurface();
	void TestSurfaceEmpty();
};

