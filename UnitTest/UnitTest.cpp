// UnitTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Rectangle_Test.h"
#include "OverlapSolver_Test.h"
#include "RectangleReaderJSon_Test.h"


using namespace std;
int main()
{
	int result = 0;
	Rectangle_Test testrectangle;
	if (testrectangle.Test() == Base_Test::result::success)
	{
		cout << "--> Rectangles Successful test.\n";
	}
	else
	{
		cout << "*** The Rectangle test has " << testrectangle.Failures() << " failed cases!!!! ***\n";
		result = -1;
	}

	OverlapSolver_Test testsolver;
	if (testsolver.Test() == Base_Test::result::success)
	{
		cout << "--> Overlap Solver Successful test.\n";
	}
	else
	{
		cout << "*** The overlap solver test has " << testsolver.Failures() << " failed cases!!!! ***\n";
		result = -1;
	}

	RectangleReaderJSon_Test testreader;
	if (testreader.Test() == Base_Test::result::success)
	{
		cout << "--> Reader Successful test.\n";
	}
	else
	{
		cout << "*** The reader test has " << testreader.Failures() << " failed cases!!!! ***\n";
		result = -1;
	}

	return result;
}

